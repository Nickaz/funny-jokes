package com.example.lab2.ui.list

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.lab2.R

class ListItemViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
    fun bindTo(listItem: ListItem) {
        val title_view = view.findViewById<TextView>(R.id.jokeTitle)
        val text_view = view.findViewById<TextView>(R.id.jokeTextPreview)

        title_view.text = listItem.title
        text_view.text = listItem.text
    }
}